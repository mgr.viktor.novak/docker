# Apache 2.4.x, PHP 7.4.x composer

FROM php:7.4-apache

ENV GMAGICK_VERSION 2.0.5RC1
ENV PHALCON_VERSION 3.3.2
ENV REDIS_VERSION 3.1.6
ENV MEMCACHED_VERSION 3.0.4

ENV HTTPD_LOG_DIR /var/log/apache2
ENV APACHE_DOCUMENT_ROOT /var/www/html

# zmena document root
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Do docker image pridame instalacne skripty
ADD _installers /tmp/installers

RUN ln -sf /usr/share/zoneinfo/Europe/Bratislava /etc/localtime \
    && apt-get update && apt-get install -y \
        locales libfreetype6-dev libjpeg62-turbo-dev libmcrypt-dev libmemcached-dev \
        libpng-dev libbz2-dev libxml2-dev libxslt1-dev libyaml-dev zlib1g-dev libzip-dev \
        libgraphicsmagick1-dev libmemcached-dev graphicsmagick mc git gcc \
        libpq-dev file re2c \
    && localedef -c -i sk_SK -f UTF-8 sk_SK.UTF-8 \
    && a2enmod expires \
    && a2enmod rewrite \
    && a2enmod vhost_alias \
    && docker-php-ext-configure gd

RUN docker-php-ext-install -j$(nproc) \
        bz2 \
        calendar \
        exif \
        gd \
        gettext \
        mysqli \
        opcache \
        pcntl \
        pdo_mysql \
        shmop \
        soap \
        sockets \
        xsl \
        zip \
        pdo \
        pdo_pgsql

RUN pecl install gmagick-$GMAGICK_VERSION \
    && pecl install igbinary \
    && pecl install msgpack \
    && pecl install xdebug \
    && pecl install yaml

RUN docker-php-ext-enable gmagick \
    && docker-php-ext-enable igbinary \
    && docker-php-ext-enable msgpack \
    && docker-php-ext-enable xdebug \
    && docker-php-ext-enable yaml \
    && bash /tmp/installers/composer.sh

# Redis
RUN pecl download redis-$REDIS_VERSION \
    && tar xzvf redis-$REDIS_VERSION.tgz \
    && cd redis-$REDIS_VERSION \
    && phpize \
    && ./configure --enable-redis-igbinary \
    && make \
    && make install \
    && docker-php-ext-enable redis

# Odstranime zdrojaky a build nastroje
RUN rm -rf /tmp/* \
    && rm -rf /var/www/html/*

ENV LANG sk_SK.UTF-8
ENV LANGUAGE=sk_SK.UTF-8
ENV LC_ALL=sk_SK.UTF-8

# Nahrame custom skripty
COPY /scripts/* /usr/local/bin/

WORKDIR /var/www/html

# Otvorime port 80
EXPOSE 80

RUN a2enmod deflate
RUN a2enmod headers

# Spustime Apache
CMD ["bash", "start-frontend"]
