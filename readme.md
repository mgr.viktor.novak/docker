# Build
```
$ cd php-7.4
$ docker build -t registry.gitlab.com/mgr.viktor.novak/docker/php:7.4-apache .
$ docker push registry.gitlab.com/mgr.viktor.novak/docker/php:7.4-apache
```
