#!/bin/bash

cd /root

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --install-dir=/usr/bin --filename=composer

rm -f composer-setup.php
